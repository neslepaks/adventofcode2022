use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

fn read_pairs() -> Vec<(u32, u32, u32, u32)> {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut result: Vec<(u32, u32, u32, u32)> = vec![];
    for line in BufReader::new(fp).lines() {
        let areas: Vec<u32> = line.expect("Cannot read line")
            .split(|c: char| !c.is_numeric())
            .map(|s| s.parse::<u32>().expect("Cannot parse int"))
            .collect();
        result.push((areas[0], areas[1], areas[2], areas[3]))
    }
    result
}

fn day4_1() {
    let pairs = read_pairs();
    let mut count: u32 = 0;
    for (elf1start, elf1stop, elf2start, elf2stop) in pairs {
        if elf1start >= elf2start && elf1stop <= elf2stop {
            // elf1 fully contained by elf2
            count += 1;
        } else if elf2start >= elf1start && elf2stop <= elf1stop {
            // elf2 fully contained by elf1
            count += 1;
        }
    }
    println!("{}", count)
}

fn day4_2() {
    let pairs = read_pairs();
    let mut count: u32 = 0;
    for (elf1start, elf1stop, elf2start, elf2stop) in pairs {
        if elf1start >= elf2start && elf1start <= elf2stop {
            count += 1;
        } else if elf2start >= elf1start && elf2start <= elf1stop {
            count += 1;
        }
    }
    println!("{}", count)
}

fn main() {
    day4_1();
    day4_2();
}
