use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

struct Crates {
    stacks: Vec<Vec<char>>,
    instructions: Vec<(u32, u32, u32)>,
}

fn read_crates() -> Crates {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut crates: Crates = Crates {
        stacks: vec![],
        instructions: vec![]
    };
    for _ in 1..=9 {
        crates.stacks.push(vec![])
    }
    let mut in_instructions: bool = false;
    for line_opt in BufReader::new(fp).lines() {
        let line = line_opt.expect("Cannot read line");
        if line.is_empty() {
            in_instructions = true;
            continue;
        }
        if !in_instructions && line.contains('[') {
            // [1] [2] [3]
            //  ^1  ^5  ^9
            for (stackno, index) in (1..line.len()).step_by(4).enumerate() {
                let chars: Vec<char> = line.chars().collect();
                if chars[index].is_alphabetic() {
                    //println!("DEBUG: pushing {} at index {} onto stack {}",
                    //         chars[index], index, stackno);
                    crates.stacks[stackno].push(chars[index])
                }
            }
        }
        if in_instructions {
            let fields: Vec<&str> = line.split_ascii_whitespace().collect();
            let mut v: Vec<u32> = vec![];
            for fieldno in [1, 3, 5] {
                v.push(fields[fieldno]
                       .parse()
                       .expect("Cannot parse number"))
            }
            crates.instructions.push((v[0], v[1], v[2]))
        }
    }
    for stackno in 0..9 {
        crates.stacks[stackno].reverse()
    }
    crates
}

fn day5_1() {
    let mut crates = read_crates();
    for (crate_count, from_stack, to_stack) in crates.instructions {
        for _ in 0..crate_count {
            let c = crates.stacks[from_stack as usize - 1].pop().expect("POP?");
            crates.stacks[to_stack as usize - 1].push(c)
        }
    }
    for stackno in 0..9 {
        let c = crates.stacks[stackno].pop().expect("POP?");
        print!("{}", c);
    }
    println!("")
}

fn day5_2() {
    let mut crates = read_crates();
    for (crate_count, from_stack, to_stack) in crates.instructions {
        let mut tmp: Vec<char> = vec![];
        for _ in 0..crate_count {
            let c = crates.stacks[from_stack as usize - 1].pop().expect("POP?");
            tmp.push(c)
        }
        for _ in 0..crate_count {
            let c = tmp.pop().expect("POP?");
            crates.stacks[to_stack as usize - 1].push(c)
        }
    }
    for stackno in 0..9 {
        let c = crates.stacks[stackno].pop().expect("POP?");
        print!("{}", c);
    }
    println!("")
}

fn main() {
    day5_1();
    day5_2();
}
