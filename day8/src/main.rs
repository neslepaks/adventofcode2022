use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

#[derive(Debug)]
struct Forest {
    trees: [[u8; 99]; 99],
}

impl Forest {
    fn visible(&self, y: usize, x: usize) -> bool {
        if y == 0 || y == 98 || x == 0 || x == 98 {
            return true;
        }
        let mut vis_left = true;
        let mut vis_up = true;
        let mut vis_down = true;
        let mut vis_right = true;
        for n in 0..y {
            if self.trees[n][x] >= self.trees[y][x] {
                vis_up = false;
            }
        }
        for n in y+1..99 {
            if self.trees[n][x] >= self.trees[y][x] {
                vis_down = false;
            }
        }
        for n in 0..x {
            if self.trees[y][n] >= self.trees[y][x] {
                vis_left = false;
            }
        }
        for n in x+1..99 {
            if self.trees[y][n] >= self.trees[y][x] {
                vis_right = false;
            }
        }
        return vis_left || vis_up || vis_down || vis_right;
    }
    fn scenic_score(&self, y: usize, x: usize) -> u32 {
        let mut left = 0;
        let mut right = 0;
        let mut down = 0;
        let mut up = 0;
        if y > 0 {
            for n in (0..y).rev() {
                up += 1;
                if self.trees[n][x] >= self.trees[y][x] {
                    break;
                }
            }
        }
        if y < 98 {
            for n in y+1..99 {
                down += 1;
                if self.trees[n][x] >= self.trees[y][x] {
                    break;
                }
            }
        }
        if x > 0 {
            for n in (0..x).rev() {
                left += 1;
                if self.trees[y][n] >= self.trees[y][x] {
                    break;
                }
            }
        }
        if x < 98 {
            for n in x+1..99 {
                right += 1;
                if self.trees[y][n] >= self.trees[y][x] {
                    break;
                }
            }
        }
        return up * down * left * right;
    }
}


fn read_forest() -> Forest {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut forest = Forest {
        trees: [[0; 99]; 99],
    };
    for (y, line_opt) in BufReader::new(fp).lines().enumerate() {
        let line_b: Vec<u8> = line_opt.expect("Line read error").bytes().map(|b| b-b'0').collect();
        for (x, val) in line_b.iter().enumerate() {
            forest.trees[y][x] = *val;
        }
    }
    forest
}

fn day8_1() {
    let forest = read_forest();
    let mut counter = 0;
    for y in 0..99 {
        for x in 0..99 {
            if forest.visible(y, x) {
                counter += 1;
            }
        }
    }
    println!("{}", counter)
}

fn day8_2() {
    let forest = read_forest();
    let mut biggest = 0;
    for y in 0..99 {
        for x in 0..99 {
            let score = forest.scenic_score(y, x);
            if score > biggest {
                biggest = score;
            }
        }
    }
    println!("{}", biggest)
}

fn main() {
    day8_1();
    day8_2();
}
