use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use std::path::Path;

struct Monkeys {
    items: Vec<Vec<String>>,
    operation: Vec<Vec<String>>,
    test: Vec<String>,
    if_true: Vec<String>,
    if_false: Vec<String>,
}

fn read_monkeys() -> io::Result<Monkeys> {
    let fp = File::open(Path::new("../input"))?;

    let mut monkeys = Monkeys {
        items: vec![],
        operation: vec![],
        test: vec![],
        if_true: vec![],
        if_false: vec![],
    };

    for line in BufReader::new(fp).lines().map(|l| l.unwrap()) {
        if line.is_empty() {
            continue;
        }
        if line.contains("Monkey") {
            // noop
        } else if line.contains("Starting items") {
            monkeys.items.push(
                line.split(':')
                    .nth(1)
                    .unwrap()
                    .split(',')
                    .map(|s| s.trim().to_string())
                    .collect())
        } else if line.contains("Operation") {
            monkeys.operation.push(
                line.split_whitespace()
                    .skip(4)
                    .map(|s| s.to_string())
                    .collect())
        } else if line.contains("Test:") {
            assert!(line.contains("divisible"));
            monkeys.test.push(
                line.split_whitespace()
                    .nth(3)
                    .unwrap()
                    .to_string())
        } else if line.contains("If true:") {
            monkeys.if_true.push(
                line.split_whitespace()
                    .skip(5)
                    .map(|s| s.to_string())
                    .collect())
        } else if line.contains("If false:") {
            monkeys.if_false.push(
                line.split_whitespace()
                    .skip(5)
                    .map(|s| s.to_string())
                    .collect())
        } else {
            panic!("Unknown line type")
        }
    }
    Ok(monkeys)
}

fn day11_1() {
    let mut monkeys = read_monkeys().expect("Monkey business");
    let mut active: Vec<i32> = vec![0; monkeys.items.len()];
    for _round in 0..20 {
        for i in 0..monkeys.items.len() {
            for old_item in monkeys.items[i].clone().iter() {
                let mut item: i32 = old_item.parse().expect("parse");

                active[i] += 1;

                match monkeys.operation[i].iter()
                        .map(|s| s.as_str()).collect::<Vec<&str>>()[..] {
                    ["+", n]        => item += n.parse::<i32>().expect("parse"),
                    ["*", "old"]    => item *= item,
                    ["*", n]        => item *= n.parse::<i32>().expect("parse"),
                    _ => unreachable!()
                }
                item /= 3;
                let to_monkey: usize = if item % monkeys.test[i]
                                         .parse::<i32>().expect("parse") == 0 {
                    monkeys.if_true[i].parse().expect("parse")
                } else {
                    monkeys.if_false[i].parse().expect("parse")
                };

                monkeys.items[to_monkey].push(item.to_string());
            }
            // Monkeys always throw all their items
            monkeys.items[i].clear();
        }
    }
    let max = active.iter().max().unwrap();
    let max2 = active.iter().filter(|&x| x != max).max().unwrap();
    println!("monkey business score: {} * {} = {}", max, max2, max*max2)
}

fn day11_2() {
    let mut monkeys = read_monkeys().expect("Monkey business");
    // MATHS! (I had to look this one up)
    let cycle = monkeys.test.iter()
                    .map(|s| s.parse::<u64>().expect("parse"))
                    .reduce(|x, y| x*y).unwrap();
    let mut active: Vec<i32> = vec![0; monkeys.items.len()];
    for _round in 0..10000 {
        for i in 0..monkeys.items.len() {
            for old_item in monkeys.items[i].clone().iter() {
                let mut item: u64 = old_item.parse().expect("parse");

                active[i] += 1;

                match monkeys.operation[i].iter()
                        .map(|s| s.as_str()).collect::<Vec<&str>>()[..] {
                    ["+", n]        => item += n.parse::<u64>().expect("parse"),
                    ["*", "old"]    => item *= item,
                    ["*", n]        => item *= n.parse::<u64>().expect("parse"),
                    _ => unreachable!()
                }
                let divisor = monkeys.test[i].parse::<u64>().expect("parse");
                let to_monkey: usize = if item % divisor == 0 {
                    monkeys.if_true[i].parse().expect("parse")
                } else {
                    monkeys.if_false[i].parse().expect("parse")
                };

                item %= cycle;
                monkeys.items[to_monkey].push(item.to_string());
            }
            // Monkeys always throw all their items
            monkeys.items[i].clear();
        }
    }
    let max = active.iter().max().unwrap();
    let max2 = active.iter().filter(|&x| x != max).max().unwrap();
    println!("monkey business score: {} * {} = {}", max, max2, *max as u64 * *max2 as u64)
}

fn main() {
    day11_1();
    day11_2();
}
