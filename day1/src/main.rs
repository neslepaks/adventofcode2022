use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

#[derive(Debug)]
struct Elf {
    calories: Vec<u32>,
}


fn read_elves() -> Vec<Elf> {
    let fp = match File::open(Path::new("../input")) {
        Err(why) => panic!("Couldn't open input: {}", why),
        Ok(fp)   => fp,
    };

    let mut elves: Vec<Elf> = vec![];
    let mut calories: Vec<u32> = vec![];
    for line in BufReader::new(fp).lines() {
        match line.expect("Line err?").as_str() {
            ""   => {
                elves.push(Elf { calories: calories.clone() });
                calories.clear();
            },
            line => calories.push(line.parse::<u32>().expect("Parse error")),
        }
    }
    elves.push(Elf { calories: calories.clone() });
    elves
}

fn day1_1() {
    let elves = read_elves();
    println!("Number of elves: {}", elves.len());
    println!("First elf: {:?}", elves[0]);
    println!("  Sum: {}", elves[0].calories.iter().sum::<u32>());
    let sums = elves.iter().map(|elf| elf.calories.iter().sum::<u32>());
    //println!("Sums: {:?}", sums);
    println!("Max: {}", sums.max().unwrap())
}

fn day1_2() {
    let elves = read_elves();
    let mut sums = elves.iter()
                        .map(|elf| elf.calories.iter().sum::<u32>())
                        .collect::<Vec<_>>();
    sums.sort();
    sums.reverse();
    let top_three = sums[0] + sums[1] + sums[2];
    println!("Top three: {}", top_three);
}

fn main() {
    day1_1();
    day1_2();
}
