use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;
use std::collections::HashSet;

fn read_rucksacks() -> Vec<(String, String)> {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut v: Vec<(String, String)> = vec![];
    for line in BufReader::new(fp).lines() {
        let l: String = line.expect("Line err?");
        let l_mid = l.len() / 2;
        let left = &l[0..l_mid];
        let right = &l[l_mid..];
        v.push((left.to_string(), right.to_string()));
    }
    v
}

fn score_item(c: char) -> u32 {
    if c.is_uppercase() {
        c.to_digit(36).expect("Input fail") - 9 + 26
    } else {
        c.to_digit(36).expect("Input fail") - 9
    }
}

fn day3_1() {
    let v = read_rucksacks();
    let mut score: u32 = 0;
    for (left, right) in v {
        let lh: HashSet<char> = HashSet::from_iter(left.chars());
        let rh: HashSet<char> = HashSet::from_iter(right.chars());
        let inter: Vec<char> = lh.intersection(&rh).copied().collect();
        score += score_item(inter[0]);
    }
    println!("{}", score);
}

fn day3_2() {
    let v = read_rucksacks();
    let mut score: u32 = 0;
    let mut set1: HashSet<char> = HashSet::new();
    let mut set2: HashSet<char> = HashSet::new();
    let mut counter: u32 = 0;
    for (left, right) in v {
        counter += 1;
        let line = [left, right].join("");
        let hs: HashSet<char> = HashSet::from_iter(line.chars());
        if counter % 3 == 0 {
            let inter: Vec<_> = hs.iter()
                .filter(|k| set1.contains(k) && set2.contains(k))
                .copied()
                .collect();
            score += score_item(inter[0]);
        }
        match counter {
            1 => { set1 = hs },
            _ => { set2 = set1; set1 = hs },
        }
    }
    println!("{}", score);
}

fn main() {
    day3_1();
    day3_2();
}
