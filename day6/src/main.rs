use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

fn read_datastream() -> String {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut line = String::new();
    BufReader::new(fp).read_line(&mut line).expect("Line read failure");
    line
}

fn find_marker(length: usize) {
    let line = read_datastream();
    for x in length..line.len() {
        let slice = &line[x-length..x];
        if slice.chars()
                .map(|c| slice.chars().filter(|x| x == &c).count())
                .all(|x| x == 1) {
            println!("{}", x);
            break
        }
    }
}

fn day6_1() {
    find_marker(4);
}

fn day6_2() {
    find_marker(14);
}

fn main() {
    day6_1();
    day6_2();
}
