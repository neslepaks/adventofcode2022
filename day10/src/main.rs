use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

#[derive(PartialEq)]
enum Op {
    Addx,
    Noop,
}

struct Instruction {
    op: Op,
    arg: Option<String>,
}

fn read_instructions() -> Vec<Instruction> {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut instructions: Vec<Instruction> = vec![];
    for line_opt in BufReader::new(fp).lines() {
        let line = line_opt.expect("Line read error");
        let fields: Vec<&str> = line.split(' ').collect();
        let op = match fields[0] {
            "addx" => Op::Addx,
            "noop" => Op::Noop,
            _ => panic!("Unknown instruction")
        };
        let arg = match op {
            Op::Addx => Some(fields[1].to_string()),
            Op::Noop => None,
        };
        instructions.push(Instruction { op, arg });
    }
    instructions
}

fn day10_1() {
    let instructions = read_instructions();
    let mut x: i32 = 1;
    let mut c: u32 = 0;
    let mut signal_strength: i32 = 0;
    for i in instructions {
        let start_instruction = c;
        loop {
            c += 1;
            if [20, 60, 100, 140, 180, 220].contains(&c) {
                signal_strength += c as i32 * x;
            }

            match (&i.op, c - start_instruction) {
                (&Op::Addx, 1) => {
                    continue;
                },
                (&Op::Addx, 2) => {
                    x += i.arg.expect("?")
                        .parse::<i32>().expect("Cannot parse arg");
                    break;
                },
                (&Op::Noop, _) => break,
                _ => panic!("Unexpected op, arg")
            }
        }
    }
    println!("{}", signal_strength);
}

fn day10_2() {
    let instructions = read_instructions();
    let mut x: i32 = 1;
    let mut c: u32 = 0;
    for i in instructions {
        let start_instruction = c;
        loop {
            match c as i32 % 40 - x {
                1 | 0 | -1 => print!("#"),
                _ => print!(".")
            }

            c += 1;

            if c % 40 == 0 { println!() }

            match (&i.op, c - start_instruction) {
                (&Op::Addx, 1) => {
                    continue;
                },
                (&Op::Addx, 2) => {
                    x += i.arg.expect("?")
                        .parse::<i32>().expect("Cannot parse arg");
                    break;
                },
                (&Op::Noop, _) => break,
                _ => panic!("Unexpected op, arg")
            }
        }
    }
}

fn main() {
    day10_1();
    day10_2();
}
