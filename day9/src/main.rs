use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;
use std::collections::HashSet;

fn read_moves() -> Vec<(char, u8)> {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut moves: Vec<(char, u8)> = vec![];
    for line_opt in BufReader::new(fp).lines() {
        let line = line_opt.expect("Line read error");
        let fields: Vec<&str> = line.split(' ').collect();
        moves.push((fields[0].chars().next().expect("?"),
                    fields[1].parse().expect("Cannot parse u8")));
    }
    moves
}

fn day9_1() {
    let moves = read_moves();
    let mut set: HashSet<(i32, i32)> = HashSet::new();
    let mut head_x: i32 = 0;
    let mut head_y: i32 = 0;
    let mut tail_x: i32 = 0;
    let mut tail_y: i32 = 0;

    set.insert((0, 0));
    for (direction, distance) in moves {
        match direction {
            'L' => head_x -= distance as i32,
            'R' => head_x += distance as i32,
            'U' => head_y -= distance as i32,
            'D' => head_y += distance as i32,
            _ => panic!("wtf")
        }
        if (head_x - tail_x).abs() <= 1 && (head_y - tail_y).abs() <= 1 {
            // no need to move tail
            continue;
        }
        let delta = match direction {
            'L' | 'R' => (head_x - tail_x).abs()-1,
            'U' | 'D' => (head_y - tail_y).abs()-1,
            _ => panic!("wtf")
        };
        for _n in 0..delta {
            match direction {
                'L' => { tail_y = head_y; tail_x -= 1; },
                'R' => { tail_y = head_y; tail_x += 1; },
                'U' => { tail_x = head_x; tail_y -= 1; },
                'D' => { tail_x = head_x; tail_y += 1; },
                _ => panic!("wtf")
            }
            set.insert((tail_y, tail_x));
        }
    }
    println!("{}", set.len())
}

#[derive(Debug, Eq, Hash, PartialEq)]
struct Point {
    x: i32,
    y: i32
}

fn calculate_tail(tail: &Point, head: &Point) -> Point {
    let x_delta = head.x - tail.x;
    let y_delta = head.y - tail.y;
    match (x_delta, y_delta) {
        (0 | 1 | -1, 0 | 1 | -1) => Point { x: tail.x, y: tail.y },
        (0 | 1 | -1, 2) => Point { x: head.x, y: head.y-1 },
        (2, 0 | 1 | -1) => Point { x: head.x-1, y: head.y },
        (0 | 1 | -1, -2) => Point { x: head.x, y: head.y+1 },
        (-2, 0 | 1 | -1) => Point { x: head.x+1, y: head.y },
        (2, 2) => Point { x: head.x-1, y: head.y-1 },
        (2, -2) => Point { x: head.x-1, y: head.y+1 },
        (-2, -2) => Point { x: head.x+1, y: head.y+1 },
        (-2, 2) => Point { x: head.x+1, y: head.y-1 },
        (x, y) => panic!("Unexpected deltas: x={}, y={}", x, y)
    }
}

fn day9_2() {
    let mut points: Vec<Point> = vec![];
    let mut tail_visits: HashSet<Point> = HashSet::new();
    tail_visits.insert(Point { x: 0, y: 0 });
    for _ in 0..10 {
        points.push(Point { x: 0, y: 0 });
    }
    let moves = read_moves();
    for (direction, distance) in moves {
        for _step in 0..distance {
            match direction {
                'L' => points[0].x -= 1,
                'R' => points[0].x += 1,
                'U' => points[0].y -= 1,
                'D' => points[0].y += 1,
                _ => panic!("wtf")
            }
            for point_no in 1..10 {
                let new = calculate_tail(&points[point_no],
                                         &points[point_no-1]);
                points[point_no].x = new.x;
                points[point_no].y = new.y;
                if point_no == 9 {
                    tail_visits.insert(new);
                }
            }
        }
    }
    println!("{}", tail_visits.len())
}

fn main() {
    day9_1();
    day9_2();
}
