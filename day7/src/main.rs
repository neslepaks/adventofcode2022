use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

#[derive(Debug, PartialEq)]
enum Nodetype {
    Directory,
    File
}

struct Node {
    name: String,
    node_type: Nodetype,
    node_size: usize,
    children: Vec<Node>
}

fn read_commands() -> Vec<String> {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut lines: Vec<String> = vec![];
    for line in BufReader::new(fp).lines() {
        lines.push(line.expect("Line read error"))
    }
    lines
}

#[derive(Debug)]
struct Fs {
    instructions: Vec<String>,
    instruction_pointer: usize,
    sum_of_100ks: usize,
    closest_to_30m: usize
}

impl Fs {
    fn new() -> Fs {
        Fs {
            instructions: read_commands(),
            instruction_pointer: 0,
            sum_of_100ks: 0,
            closest_to_30m: 70_000_000
        }
    }

    fn build_tree(&mut self, cwd: &mut Node) {
        while self.instruction_pointer < self.instructions.len() {
            let line = &self.instructions[self.instruction_pointer];
            self.instruction_pointer += 1;
            match line.split(' ').collect::<Vec<_>>()[0..] {
                ["$", "cd", "/"] => (),
                ["$", "cd", ".."] => break,
                ["$", "cd", dir] => {
                    let pos = cwd.children.iter()
                        .position(|n| n.name == dir).expect("Not found in listing?");
                    self.build_tree(&mut cwd.children[pos]);
                },
                ["$", "ls"] => (),
                ["dir", name] => {
                    cwd.children.push(Node {
                        name: name.to_string(),
                        node_type: Nodetype::Directory,
                        node_size: 0,
                        children: vec![],
                    });
                },
                [size, name] => {
                    cwd.children.push(Node {
                        name: name.to_string(),
                        node_type: Nodetype::File,
                        node_size: size.parse().expect("File size parse error"),
                        children: vec![],
                    });
                },
                _ => panic!("Unexpected input")
            }
        }
    }

    fn sizes(&mut self, root: &Node, get_closest_to: Option<usize>) -> usize {
        let mut this_node_size: usize = 0;
        for child in &root.children {
            if child.node_type == Nodetype::File {
                this_node_size += child.node_size;
            } else {
                let sz = self.sizes(child, get_closest_to);
                this_node_size += sz;
            }
        }
        if this_node_size <= 100_000 {
            self.sum_of_100ks += this_node_size;
        }
        if let Some(closest_to) = get_closest_to {
            if this_node_size > closest_to && this_node_size < self.closest_to_30m {
                self.closest_to_30m = this_node_size;
            }
        }
        this_node_size
    }
}

fn print_tree(root: &Node, indent: usize) {
    println!("{:>width$}{} ({:?}) = {}", "", root.name,
        root.node_type, root.node_size, width = indent);
    for child in &root.children {
        print_tree(child, indent+2);
    }
}

fn day7_1() {
    let mut fs = Fs::new();
    let mut root = Node {
        name:       String::from("/"),
        node_type:  Nodetype::Directory,
        node_size:  0,
        children:   vec![]
    };
    fs.build_tree(&mut root);
    print_tree(&root, 0);
    let total = fs.sizes(&root, None);
    println!("sum of 100ks: {} (total size: {})", fs.sum_of_100ks, total);
}

fn day7_2() {
    let mut fs = Fs::new();
    let mut root = Node {
        name:       String::from("/"),
        node_type:  Nodetype::Directory,
        node_size:  0,
        children:   vec![]
    };
    fs.build_tree(&mut root);
    let total = fs.sizes(&root, None);
    let threshold = 30_000_000 - (70_000_000 - total);
    // Just run it again, Bob
    fs.sum_of_100ks = 0;
    fs.sizes(&root, Some(threshold));
    println!("closest to threshold ({}) = {}", threshold, fs.closest_to_30m)
}

fn main() {
    day7_1();
    day7_2();
}
