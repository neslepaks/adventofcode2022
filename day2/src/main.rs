use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

fn read_strategy() -> Vec<(char, char)> {
    let fp = File::open(Path::new("../input")).expect("Couldn't open input");

    let mut v: Vec<(char, char)> = vec![];
    for line in BufReader::new(fp).lines() {
        let chars: Vec<char> = line.expect("Line err?").chars().collect();
        v.push((chars[0], chars[2]))
    }
    v
}

fn score_game(play: char, strat: char) -> u32 {
    match play {
        'A' => match strat {
            'X' => 3,
            'Y' => 6,
            _ => 0
        },
        'B' => match strat {
            'Y' => 3,
            'Z' => 6,
            _ => 0
        },
        'C' => match strat {
            'Z' => 3,
            'X' => 6,
            _ => 0
        },
        _ => panic!("Unknown char: {}", play)
    }
}

fn day2_1() {
    let strategy = read_strategy();
    let mut score: u32 = 0;
    for (play, strat) in strategy {
        score += match strat {
            'X' => 1,
            'Y' => 2,
            'Z' => 3,
            _ => panic!("Unknown char: {}", strat)
        };
        score += score_game(play, strat);
    }
    println!("{}", score)
}

fn adjust_strategy(play: char, strat_input: char) -> char {
    match (strat_input, play) {
        // Lose
        ('X', 'A') => 'Z',
        ('X', 'B') => 'X',
        ('X', 'C') => 'Y',
        // Draw
        ('Y', 'A') => 'X',
        ('Y', 'B') => 'Y',
        ('Y', 'C') => 'Z',
        // Win
        ('Z', 'A') => 'Y',
        ('Z', 'B') => 'Z',
        ('Z', 'C') => 'X',
        (_, _) => panic!("WTF")
    }
}

fn day2_2() {
    let strategy = read_strategy();
    let mut score: u32 = 0;
    for (play, strat_input) in strategy {
        let strat = adjust_strategy(play, strat_input);
        score += match strat {
            'X' => 1,
            'Y' => 2,
            'Z' => 3,
            _ => panic!("Unknown char: {}", strat)
        };
        score += score_game(play, strat);
    }
    println!("{}", score)
}

fn main() {
    day2_1();
    day2_2();
}
